/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.hudson.utils;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.jira.bc.admin.ApplicationPropertiesService;
import com.atlassian.jira.bc.admin.ApplicationProperty;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;

/**
 * Testcase for the {@link ChangelogAnnotator}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 5.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class ChangelogAnnotatorTest {

	private static final String JIRA_BASE_URL = "http://issues.marvelution.com";

	@Mock private MutableIssue issue;
	@Mock private IssueManager issueManager;
	@Mock private ApplicationPropertiesService applicationPropertiesService;
	@Mock private ApplicationProperties applicationProperties;

	private ChangelogAnnotator annotator;

	/**
	 * Setup the testcase
	 * 
	 * @throws Exception in case of errors
	 */
	@Before
	public void setup() throws Exception {
		when(applicationPropertiesService.getApplicationProperty(APKeys.JIRA_PROJECTKEY_PATTERN)).thenReturn(new ApplicationProperty(null, "[a-zA-Z][a-zA-Z0-9_]+"));
		when(applicationProperties.getString(APKeys.JIRA_BASEURL)).thenReturn(JIRA_BASE_URL);
		annotator = new ChangelogAnnotator(applicationPropertiesService, issueManager) {

			/**
			 * {@inheritDoc}
			 */
			@Override
			ApplicationProperties getApplicationProperties() {
				return applicationProperties;
			}

		};
	}

	/**
	 * Test the Changelog Annotation with a single existing issue key
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testReplaceSingleExistingIssueKeyGivenBaseURL() throws Exception {
		when(issueManager.getIssueObject("MARVJIRAHUDSON-261")).thenReturn(issue);
		String changelog = "Implemented fix for issue MARVJIRAHUDSON-261";
		String result = annotator.annotate(JIRA_BASE_URL, changelog);
		assertThat(result, is("Implemented fix for issue <a href=\"" + JIRA_BASE_URL
			+ "/browse/MARVJIRAHUDSON-261\">MARVJIRAHUDSON-261</a>"));
		verify(applicationPropertiesService).getApplicationProperty(APKeys.JIRA_PROJECTKEY_PATTERN);
		verifyZeroInteractions(applicationProperties);
	}

	/**
	 * Test the Changelog Annotation with a single existing issue key
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testReplaceSingleExistingIssueKey() throws Exception {
		when(applicationProperties.getString(APKeys.JIRA_BASEURL)).thenReturn(JIRA_BASE_URL);
		when(issueManager.getIssueObject("MARVJIRAHUDSON-261")).thenReturn(issue);
		String changelog = "Implemented fix for issue MARVJIRAHUDSON-261";
		String result = annotator.annotate(changelog);
		assertThat(result, is("Implemented fix for issue <a href=\"" + JIRA_BASE_URL
			+ "/browse/MARVJIRAHUDSON-261\">MARVJIRAHUDSON-261</a>"));
		verify(applicationPropertiesService).getApplicationProperty(APKeys.JIRA_PROJECTKEY_PATTERN);
		verify(applicationProperties).getString(APKeys.JIRA_BASEURL);
	}

	/**
	 * Test the Changelog Annotation with a single existing issue key that is used twice
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testReplaceSingleDoubleExistingIssueKey() throws Exception {
		when(issueManager.getIssueObject("MARVJIRAHUDSON-261")).thenReturn(issue);
		String changelog = "MARVJIRAHUDSON-261; Implemented fix for issue MARVJIRAHUDSON-261";
		String result = annotator.annotate(JIRA_BASE_URL, changelog);
		assertThat(result, is("<a href=\"" + JIRA_BASE_URL
			+ "/browse/MARVJIRAHUDSON-261\">MARVJIRAHUDSON-261</a>; Implemented fix for issue <a href=\""
			+ JIRA_BASE_URL + "/browse/MARVJIRAHUDSON-261\">MARVJIRAHUDSON-261</a>"));
		verify(applicationPropertiesService).getApplicationProperty(APKeys.JIRA_PROJECTKEY_PATTERN);
		verifyZeroInteractions(applicationProperties);
	}

	/**
	 * Test the Changelog Annotation with multiple existing issue keys
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testReplaceMultipleExistingIssueKeys() throws Exception {
		when(issueManager.getIssueObject("MARVJIRAHUDSON-261")).thenReturn(issue);
		when(issueManager.getIssueObject("MARVJIRAHUDSON-262")).thenReturn(issue);
		String changelog = "Implemented fix for issue MARVJIRAHUDSON-261 and MARVJIRAHUDSON-262";
		String result = annotator.annotate(JIRA_BASE_URL, changelog);
		assertThat(result, is("Implemented fix for issue <a href=\"" + JIRA_BASE_URL
			+ "/browse/MARVJIRAHUDSON-261\">MARVJIRAHUDSON-261</a> and <a href=\""
			+ JIRA_BASE_URL + "/browse/MARVJIRAHUDSON-262\">MARVJIRAHUDSON-262</a>"));
		verify(applicationPropertiesService).getApplicationProperty(APKeys.JIRA_PROJECTKEY_PATTERN);
		verifyZeroInteractions(applicationProperties);
	}

	/**
	 * Test the Changelog Annotation with existing and missing issue keys
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testReplaceExistingAndMissingIssueKeys() throws Exception {
		when(issueManager.getIssueObject("MARVJIRAHUDSON-261")).thenReturn(issue);
		when(issueManager.getIssueObject("MARVJIRAHUDSON-262")).thenReturn(null);
		String changelog = "Implemented fix for issue MARVJIRAHUDSON-261 and MARVJIRAHUDSON-262";
		String result = annotator.annotate(JIRA_BASE_URL, changelog);
		assertThat(result, is("Implemented fix for issue <a href=\"" + JIRA_BASE_URL
			+ "/browse/MARVJIRAHUDSON-261\">MARVJIRAHUDSON-261</a> and MARVJIRAHUDSON-262"));
		verify(applicationPropertiesService).getApplicationProperty(APKeys.JIRA_PROJECTKEY_PATTERN);
		verifyZeroInteractions(applicationProperties);
	}

}
