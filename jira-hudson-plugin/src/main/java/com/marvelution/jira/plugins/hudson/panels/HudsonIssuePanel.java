/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.hudson.panels;

import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueTabPanel2;
import com.atlassian.jira.plugin.issuetabpanel.GetActionsReply;
import com.atlassian.jira.plugin.issuetabpanel.GetActionsRequest;
import com.atlassian.jira.plugin.issuetabpanel.ShowPanelReply;
import com.atlassian.jira.plugin.issuetabpanel.ShowPanelRequest;
import com.marvelution.jira.plugins.hudson.panels.utils.HudsonPanelHelper;

/**
 * {@link AbstractIssueTabPanel} implementation for Hudson Builds
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class HudsonIssuePanel extends AbstractIssueTabPanel2 {

	private final HudsonPanelHelper panelHelper;

	/**
	 * Constructor
	 * 
	 * @param panelHelper the {@link HudsonPanelHelper} helper class
	 */
	public HudsonIssuePanel(HudsonPanelHelper panelHelper) {
		this.panelHelper = panelHelper;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public GetActionsReply getActions(GetActionsRequest request) {
		return GetActionsReply.create(
			new HudsonIssuePanelAction(descriptor(), panelHelper, request.issue(), request.remoteUser())
		);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ShowPanelReply showPanel(ShowPanelRequest request) {
		return ShowPanelReply.create(panelHelper.showPanel(request.issue(), request.remoteUser()));
	}

}
